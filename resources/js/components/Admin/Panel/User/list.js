import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import {
    Collapse,
    Row,
    Col,
    Typography,
    Button,
    Table,
    Space,
    Tag,
    Drawer,
    Input,
    Switch
} from "antd";
import { SearchOutlined, AudioOutlined } from "@ant-design/icons";

const { Title } = Typography;
const { Panel } = Collapse;
const { Search } = Input;

import Axios from "axios";

const List = () => {
    const history = useHistory();
    const { sectionId } = useParams();
    const [users, setUsers] = React.useState([]);

    React.useEffect(() => {
        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
        Axios.get(`/api/users`)
            .then(response => {
                // console.log(response);
                const { status, data } = response;
                if (status == 200) {
                    setUsers(data);
                } else {
                    alert("Error !");
                }
            })
            .catch(error => {
                alert(error);
            });
        }
    }, [sectionId]);

    const handleDelete = async (id, e) => {
        if (window.confirm("yakin mau dihapus?")) {
            try {
                await Axios.delete("/api/user/delete/" + id);
                alert("data berhasil dihapus");
                // setTaks(tasks.filter(task => section.id !== id));
                e.preventDefault();
                // setTaks(tasks.filter(task => task.id !== id));
            } catch (error) {
                alert("Network error");
            }
        }
    };

    const toUpdate = async (id, e) => {
        history.push(`/admin/users/edit/${id}`);
    };

    const columns = [
        {
            title: "ID",
            dataIndex: "id",
            sorter: (a, b) => a.id.length - b.id.length,
            sortDirections: ["descend", "ascend"]
        },
        {
            title: "Name",
            dataIndex: "name",
            sorter: (a, b) => a.name.length - b.name.length,
            sortDirections: ["descend", "ascend"]
        },
        {
            title: "Email",
            dataIndex: "email",
            sorter: (a, b) => a.email.length - b.email.length,
            sortDirections: ["descend", "ascend"]
        },
        {
            title: "Verified",
            key: "email_verified_at",
            sorter: (a, b) =>
                a.email_verified_at.length - b.email_verified_at.length,
            sortDirections: ["descend", "ascend"],
            render: (text, record) => {
                if (record.email_verified_at != null) {
                    return <Tag color="green">OK</Tag>;
                } else {
                    return <Tag color="red">X</Tag>;
                }
            }
        },
        // {
        //     title: "Status",
        //     key: "is_done",
        //     sorter: (a, b) => a.is_done - b.is_done,
        //     sortDirections: ["descend", "ascend"],
        //     render: (text, record) => {
        //         if (record.is_done == 1) {
        //             return <Tag color="green">FINISH</Tag>;
        //         } else {
        //             return <Tag color="red">NOT YET</Tag>;
        //         }
        //     }
        // },
        // {
        //     title: "Time Finish",
        //     key: "done_at",
        //     sorter: (a, b) =>
        //         moment(a.done_at).unix() - moment(b.done_at).unix(),
        //     sortDirections: ["descend", "ascend"],
        //     render: (text, record) => {
        //         // console.log(record);
        //         if (!record.done_at) {
        //             return <p>Invalid Date because Not Yet</p>;
        //         } else {
        //             return moment(record.done_at).from(record.created_at);
        //         }
        //     }
        // },
        {
            title: "Action",
            key: "action",
            render: (text, record) => (
                <Space size="middle">
                    <a
                        onClick={e => {
                            toUpdate(record.id, e);
                        }}
                    >
                        Update
                    </a>
                    <a
                        onClick={e => {
                            handleDelete(record.id, e);
                        }}
                    >
                        Delete
                    </a>
                </Space>
            )
        }
    ];
    return (
        <>
            <Row>
                <Col span={20} offset={2}>
                    <Title level={2}>List Taks</Title>
                    <a href={`/admin/users/add`}>
                        <Button type="primary" className={"add-task"}>
                            Add task
                        </Button>
                    </a>
                    <hr />
                    <Table columns={columns} dataSource={users} rowKey="Id"/>
                </Col>
            </Row>
        </>
    );
};

export default List;
