import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Input, Card } from "antd";
import { AudioOutlined } from "@ant-design/icons";
import Cookies from "js-cookie";

const { Title } = Typography;
const { Panel } = Collapse;
const { Search } = Input;

import Axios from "axios";

const List = () => {
    const [user, setUser] = React.useState([]);
    const { userId } = useParams();
    const history = useHistory();
    const logData = {
        email: localStorage.getItem("email"),
        token: localStorage.getItem("token")
    };

    React.useEffect(() => {
        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
            if (userId == "after-log") {
                Axios.post(`/api/me`, logData)
                    .then(response => {
                        // console.log(response);
                        const { data, status } = response;

                        console.log(data);

                        if (status == 200) {
                            setUser(data);
                            console.log(user);
                        } else {
                            alert("Jaringan Error / Gagal");
                        }
                    })
                    .catch(error => {
                        alert(error);
                    });
            } else {
                Axios.get(`/api/user/${userId}`)
                    .then(response => {
                        // console.log(response);
                        const { data, status } = response;

                        console.log(data);

                        if (status == 200) {
                            setUser(data);
                            console.log(user);
                        } else {
                            alert("Jaringan Error / Gagal");
                        }
                    })
                    .catch(error => {
                        alert(error);
                    });
            }
        }
    }, []);

    const logout = () => {
        Cookies.remove("login");
        history.push(`/user/login`);
    };

    return (
        <Row>
            <Col span={20} offset={2}>
                <Title level={2}>Profile User</Title>
                <a href="/">
                    <Button type="primary" className={"add-section"}>
                        Home
                    </Button>
                </a>
                <a href="#" onClick={logout}>
                    <Button type="primary" className={"button-logout"}>
                        Logout
                    </Button>
                </a>
                <Col span={12}>{/* <InputSearch /> */}</Col>
                <hr />
                <Card
                    title="User Profile"
                    className={"profile-card"}
                    style={{ width: 300 }}
                >
                    <p>Name : {user.name}</p>
                    <p>Email : {user.email}</p>
                </Card>
            </Col>
        </Row>
    );
};

export default List;

// if (document.getElementById("root")) {
//     ReactDOM.render(<App />, document.getElementById("root"));
// }
