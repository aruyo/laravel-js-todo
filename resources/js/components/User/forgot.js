import React from "react";
import ReactDOM from "react-dom";
import { useHistory } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Add = () => {
    const history = useHistory();
    const [user, setUser] = React.useState({
        email: ""
    });

    const handleChange = (e, name) => {
        const value = e.target.value;
        setUser({
            ...user,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.post("/api/forgot", user);

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("please check your email");
                // history.push(`/user/${response.data.id}`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Please input your user!");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Request Change Password</Title>
                <hr />
                <br />
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                >
                    <Form.Item label="Email" name="email">
                        <Input
                            value={user.email}
                            onChange={e => handleChange(e, "email")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Add;
