<?php

namespace App\Http\Controllers;

use App\Task;
use App\Section;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Task::with('section')->get();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Task;
        $data->id_section = $request->get('id_section');
        $data->description = $request->get('description');
        $data->is_done = $request->get('is_done');
        $data->done_at = $request->get('done_at');
        $data->save();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Task::findOrFail($id);

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Task::findOrFail($id);
        $data->id_section = $request->get('id_section');
        $data->description = $request->get('description');
        $data->is_done = $request->get('is_done');
        $data->done_at = $request->get('done_at');
        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Task::findOrFail($id);
        $data->delete();

        return response()->json($data);
    }

    public function search(Request $request, $id)
    {
        $query = $request->input('query');
        if ($query)
            $data = Task::where('id_section', $id)->where('description', 'like', '%' . $query . '%')->get();

        return response()->json($data);
    }

    public function filter(Request $request, $id)
    {
        $query = $request->input('status');
        if ($query == 1 || $query == 'true') {
            $data = Task::where('id_section', $id)->where('is_done', true)->get();
        } else {
            $data = Task::where('id_section', $id)->where('is_done', false)->get();
        }

        return response()->json($data);
    }
}
