import React from "react";
import ReactDOM from "react-dom";
import { useHistory } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";
import Cookies from "js-cookie";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Add = () => {
    const history = useHistory();
    const [section, setSection] = React.useState({
        description: ""
    });

    if (!Cookies.get("login")) {
        history.push(`/user/login`);
    }

    const handleChange = (e, name) => {
        const value = e.target.value;
        setSection({
            ...section,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.post("/api/section", section);

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("tambah section berhasil");
                history.push("/");
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Please input your Section!");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Add New Section</Title>
                <hr />
                <br />
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                >
                    <Form.Item label="Description" name="description">
                        <Input
                            value={section.description}
                            onChange={e => handleChange(e, "description")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                        <a href="/" className={"back-button"}>
                            <Button type="primary" className={"add-section"}>
                                Back
                            </Button>
                        </a>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Add;
