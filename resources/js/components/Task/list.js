import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import {
    Collapse,
    Row,
    Col,
    Typography,
    Button,
    Table,
    Space,
    Tag,
    Drawer,
    Input,
    Switch
} from "antd";
import { SearchOutlined, AudioOutlined } from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import moment from "moment";

const { Title } = Typography;
const { Panel } = Collapse;
const { Search } = Input;

import Axios from "axios";
import Cookies from "js-cookie";

// function onChange(pagination, filters, sorter, extra) {
//     console.log("params", pagination, filters, sorter, extra);
// }

const List = () => {
    const history = useHistory();
    const { sectionId } = useParams();
    const [sections, setSections] = React.useState([]);
    const [tasks, setTaks] = React.useState([]);
    const [fill, setFill] = React.useState({
        id_section: sectionId,
        description: "",
        is_done: 0,
        done_at: null
    });

    React.useEffect(() => {
        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
        Axios.get(`/api/section/${sectionId}/task`)
            .then(response => {
                // console.log(response);
                const { status, data } = response;
                if (status == 200) {
                    setSections(data);
                    setTaks(data[0].tasks);
                    // console.log(response.data[0].tasks);
                    // console.log(tasks);
                } else {
                    alert("Error !");
                }
            })
            .catch(error => {
                alert(error);
            });
        }
    }, [sectionId]);

    const handleDelete = async (id, e) => {
        if (window.confirm("yakin mau dihapus?")) {
            try {
                await Axios.delete("/api/task/" + id);
                alert("data berhasil dihapus");
                // setTaks(tasks.filter(task => section.id !== id));
                e.preventDefault();
                setTaks(tasks.filter(task => task.id !== id));
            } catch (error) {
                alert("Network error");
            }
        }
    };

    const toUpdate = async (id, e) => {
        history.push(`/edit-task/${sectionId}/task/${id}`);
    };

    const onFinish = async (id, e, desc) => {
        try {
            const response = await Axios.put(`/api/task/${id}`, {
                id_section: sectionId,
                description: desc,
                is_done: 1,
                done_at: moment().format("YYYY-MM-DD HH:mm:ss")
            });

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("update task berhasil");
                window.location.reload();
                history.push(`/section/${sectionId}`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Network error");
        }
    };

    const columns = [
        {
            title: "Description",
            dataIndex: "description",
            sorter: (a, b) => a.description.length - b.description.length,
            sortDirections: ["descend", "ascend"]
        },
        {
            title: "Status",
            key: "is_done",
            sorter: (a, b) => a.is_done - b.is_done,
            sortDirections: ["descend", "ascend"],
            render: (text, record) => {
                if (record.is_done == 1) {
                    return <Tag color="green">FINISH</Tag>;
                } else {
                    return <Tag color="red">NOT YET</Tag>;
                }
            }
        },
        {
            title: "Time Finish",
            key: "done_at",
            sorter: (a, b) =>
                moment(a.done_at).unix() - moment(b.done_at).unix(),
            sortDirections: ["descend", "ascend"],
            render: (text, record) => {
                // console.log(record);
                if (!record.done_at) {
                    return <p>Invalid Date because Not Yet</p>;
                } else {
                    return moment(record.done_at).from(record.created_at);
                }
            }
        },
        {
            title: "Task Created",
            key: "created_at",
            sorter: (a, b) =>
                moment(a.created_at).unix() - moment(b.created_at).unix(),
            sortDirections: ["descend", "ascend"],
            render: (text, record) => {
                return moment(record.created_at).format("LLLL");
            }
        },
        {
            title: "Action",
            key: "action",
            render: (text, record) => (
                <Space size="middle">
                    <a
                        onClick={e => {
                            onFinish(record.id, e, record.description);
                        }}
                    >
                        Finish
                    </a>
                    <a
                        onClick={e => {
                            toUpdate(record.id, e);
                        }}
                    >
                        Update
                    </a>
                    <a
                        onClick={e => {
                            handleDelete(record.id, e);
                        }}
                    >
                        Delete
                    </a>
                </Space>
            )
        }
    ];

    const InputSearch = () => {
        const suffix = (
            <AudioOutlined
                style={{
                    fontSize: 16,
                    color: "#1890ff"
                }}
            />
        );

        const onSearch = value => {
            Axios.get(`/api/search-task/${sectionId}/?query=${value}`)
                .then(response => {
                    // console.log(response);
                    const { data, status } = response;

                    // console.log(data);

                    if (status == 200) {
                        console.log(data);
                        setTaks(data);
                    } else {
                        alert("Jaringan Error / Gagal");
                    }
                })
                .catch(error => {
                    alert(error);
                });
        };

        return (
            <>
                <Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={onSearch}
                />
            </>
        );
    };

    const onChange = checked => {
        if (checked === true) {
            Axios.get(`/api/filter-task/${sectionId}/?status=true`)
                .then(response => {
                    // console.log(response);
                    const { data, status } = response;

                    // console.log(data);

                    if (status == 200) {
                        console.log(data);
                        setTaks(data);
                    } else {
                        alert("Jaringan Error / Gagal");
                    }
                })
                .catch(error => {
                    alert(error);
                });
        }

        if (checked === false) {
            Axios.get(`/api/filter-task/${sectionId}/?status=false`)
                .then(response => {
                    // console.log(response);
                    const { data, status } = response;

                    // console.log(data);

                    if (status == 200) {
                        console.log(data);
                        setTaks(data);
                    } else {
                        alert("Jaringan Error / Gagal");
                    }
                })
                .catch(error => {
                    alert(error);
                });
        }
    };

    const clearFilter = () => {
        Axios.get(`/api/section/${sectionId}/task`)
            .then(response => {
                // console.log(response);
                const { status, data } = response;
                if (status == 200) {
                    setSections(data);
                    setTaks(data[0].tasks);
                    // console.log(response.data[0].tasks);
                    // console.log(tasks);
                } else {
                    alert("Error !");
                }
            })
            .catch(error => {
                alert(error);
            });
    };
    return (
        <>
            <Row>
                <Col span={20} offset={2}>
                    <Title level={2}>List Taks</Title>
                    <a href={`/add-task/${sectionId}`}>
                        <Button type="primary" className={"add-task"}>
                            Add task
                        </Button>
                    </a>
                    <Row>
                        <Col span={12}>
                            <InputSearch />
                        </Col>
                        <Col span={12}>
                            <Row>
                                <h2 className={"filter-title"}>
                                    Filter By State :
                                </h2>
                                <Switch
                                    className={"switch-filter"}
                                    checkedChildren="FINISH"
                                    unCheckedChildren="NOT YET"
                                    onChange={onChange}
                                    defaultChecked
                                />
                                <Button
                                    type="primary"
                                    className={"button-filter"}
                                    onClick={clearFilter}
                                >
                                    Clear
                                </Button>

                                <a href="/">
                                    <Button
                                        type="primary"
                                        className={"button-back-task"}
                                    >
                                        Back
                                    </Button>
                                </a>
                            </Row>
                        </Col>
                    </Row>

                    <hr />
                    <Table columns={columns} dataSource={tasks} rowKey="Id" />
                </Col>
            </Row>
        </>
    );
};

export default List;
