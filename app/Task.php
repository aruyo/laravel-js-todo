<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function section()
    {
        return $this->belongsTo(Section::class, 'id_section', 'id');
    }
}
