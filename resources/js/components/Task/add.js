import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";
import Cookies from "js-cookie";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Add = () => {
    const history = useHistory();
    const { sectionId } = useParams();
    const [task, setTask] = React.useState({
        id_section: sectionId,
        description: ""
    });

    if (!Cookies.get("login")) {
        history.push(`/user/login`);
    }

    const handleChange = (e, name) => {
        const value = e.target.value;
        setTask({
            ...task,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.post("/api/task", task);

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("tambah task berhasil");
                history.push(`/section/${sectionId}`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Please input your task!");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Add New Task</Title>
                <hr />
                <br />
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                >
                    <Form.Item label="Description" name="description">
                        <Input
                            value={task.description}
                            onChange={e => handleChange(e, "description")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                        <a
                            href={`/section/${sectionId}`}
                            className={"back-button"}
                        >
                            <Button type="primary" className={"add-task"}>
                                Back
                            </Button>
                        </a>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Add;
