import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import ListSection from "./Section/list";
import AddSection from "./Section/add";
import EditSection from "./Section/edit";
import ShowSectionTask from "./Task/list";
import EditTask from "./Task/edit";
import AddTask from "./Task/add";
import RegisterUser from "./User/register";
import ProfileUser from "./User/profile";
import LoginUser from "./User/login";
import ForgotUser from "./User/forgot";
import ChangePassword from "./User/changePassword";
import AdminUsers from "../components/Admin/Panel/User/list";
import AdminAddUser from "../components/Admin/Panel/User/add";
import AdminEditUser from "../components/Admin/Panel/User/edit";

import "antd/dist/antd.css";
import "../../../public/css/custom.css";

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={ListSection} />
                <Route path="/add-section" exact component={AddSection} />
                <Route
                    path="/edit-section/:sectionId"
                    exact
                    component={EditSection}
                />
                <Route
                    path="/section/:sectionId"
                    exact
                    component={ShowSectionTask}
                />
                <Route
                    path="/edit-task/:sectionId/task/:taskId"
                    exact
                    component={EditTask}
                />
                <Route path="/add-task/:sectionId" exact component={AddTask} />
                <Route path="/user/register" exact component={RegisterUser} />
                <Route path="/user/login" exact component={LoginUser} />
                <Route
                    path="/user/profile/:userId"
                    exact
                    component={ProfileUser}
                />
                <Route path="/user/forgot" exact component={ForgotUser} />
                <Route path="/reqpassword" exact component={ChangePassword} />
                <Route path="/admin/users" exact component={AdminUsers} />
                <Route path="/admin/users/add" exact component={AdminAddUser} />
                <Route path="/admin/users/edit/:userId" exact component={AdminEditUser} />
            </Switch>
        </BrowserRouter>
    );
}

export default App;

if (document.getElementById("root")) {
    ReactDOM.render(<App />, document.getElementById("root"));
}
