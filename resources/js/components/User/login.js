import React from "react";
import ReactDOM from "react-dom";
import { useHistory } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";
import Cookies from "js-cookie";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Add = () => {
    const history = useHistory();
    const [user, setUser] = React.useState({
        email: "",
        password: ""
    });

    const handleChange = (e, name) => {
        const value = e.target.value;
        setUser({
            ...user,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.post("/api/auth/login", user);

            console.log(response);

            const { status, message } = response.data;
            if (response.status == 200) {
                // alert("tambah user berhasil");
                localStorage.setItem("token", response.data.access_token);
                localStorage.setItem("email", user.email);
                Cookies.set("login", user.email);
                console.log(response.data);
                history.push(`/user/profile/after-log`);
                // history.push(`/user/${response.data.id}`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Incorrect Email or Password!");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Login User</Title>
                <hr />
                <br />
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                >
                    <Form.Item label="Email" name="email">
                        <Input
                            value={user.email}
                            onChange={e => handleChange(e, "email")}
                        />
                    </Form.Item>
                    <Form.Item label="Password" name="password">
                        <Input.Password
                            value={user.password}
                            onChange={e => handleChange(e, "password")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <a href="/user/forgot">Forgot Password? </a>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                        <a href="/user/register" className={"back-button"}>
                            <Button type="primary" className={"add-user"}>
                                Register
                            </Button>
                        </a>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Add;
