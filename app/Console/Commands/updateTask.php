<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;
use Illuminate\Support\Carbon;

class updateTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Status Task from Artisan Command Line';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Input the Task ID : ";
        $ID = trim(fgets(STDIN));
        echo "Status ? ";
        $status = trim(fgets(STDIN));
        if ($status == 'finish' || $status == 'FINISH') {
            $dt = Carbon::now();
            $dateNow = $dt->toDateTimeString();

            $data = Task::findOrFail($ID);
            $data->is_done = 1;
            $data->done_at = $dateNow;
            $data->save();

            $this->info('Task status has been change!');
        } elseif ($status == 'not yet' || $status == 'NOT YET') {
            $data = Task::findOrFail($ID);
            $data->is_done = 0;
            $data->done_at = '';
            $data->save();
            $this->info('Task status has been change!');
        } else {
            $this->info('Error !!!');
        }
    }
}
