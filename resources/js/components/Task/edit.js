import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import {
    Collapse,
    Row,
    Col,
    Typography,
    Button,
    Form,
    Input,
    Select,
    DatePicker
} from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";
import moment from "moment";
import Cookies from "js-cookie";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Edit = () => {
    const history = useHistory();
    const { taskId, sectionId } = useParams();
    const [task, setTask] = React.useState({
        description: "",
        is_done: 0,
        done_at: null
    });
    const [form] = Form.useForm();

    React.useEffect(() => {

        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
        Axios.get(`/api/task/${taskId}`)
            .then(response => {
                // console.log(response);
                const { status, data } = response;
                let statusTask;
                if (data.is_done === 1) {
                    statusTask = "FINISH";
                } else {
                    statusTask = "NOT YET";
                }
                let dateNull;
                if (!data.done_at) {
                    dateNull = null;
                } else {
                    dateNull = moment(data.done_at);
                }
                if (status == 200) {
                    setTask(data);
                    console.log(data);
                    form.setFieldsValue({
                        description: data.description,
                        is_done: statusTask,
                        done_at: dateNull
                    });
                } else {
                    alert("Error !");
                }
            })
            .catch(error => {
                alert(error);
            });
        }
    }, [taskId]);

    const SelectChange = (value, name) => {
        setTask({
            ...task,
            [name]: value
        });
    };

    const handleChange = (e, name) => {
        const value = e.target.value;
        setTask({
            ...task,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.put(`/api/task/${taskId}`, task);

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("update task berhasil");
                history.push(`/section/${sectionId}`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Network error");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Edit task</Title>
                <hr />
                <Form {...layout} form={form}>
                    <Form.Item label="Description" name="description">
                        <Input
                            type="text"
                            onChange={e => handleChange(e, "description")}
                        />
                    </Form.Item>

                    {/* <Form.Item label="Status" name="is_done">
                        <Input
                            type="text"
                            onChange={e => handleChange(e, "is_done")}
                        />
                    </Form.Item> */}

                    <Form.Item name="is_done" label="Status">
                        <Select
                            placeholder="Please select a Status"
                            // onChange={e => handleChange(e, "is_done")}
                            onChange={value => SelectChange(value, "is_done")}
                        >
                            <Option value="1">FINISH</Option>
                            <Option value="2">NOT YET</Option>
                        </Select>
                    </Form.Item>

                    {/* <Form.Item label="Time" name="done_at">
                        <Input
                            type="text"
                            onChange={e => handleChange(e, "done_at")}
                        />
                    </Form.Item> */}

                    <Form.Item name="done_at" label="Time Finish">
                        <DatePicker
                            showTime
                            format="YYYY-MM-DD HH:mm:ss"
                            onChange={value =>
                                SelectChange(
                                    moment(value).format("YYYY-MM-DD HH:mm:ss"),
                                    "done_at"
                                )
                            }
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                        <a
                            href={`/section/${sectionId}`}
                            className={"back-button"}
                        >
                            <Button type="primary" className={"add-task"}>
                                Back
                            </Button>
                        </a>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Edit;

// if (document.getElementById("root")) {
//     ReactDOM.render(<App />, document.getElementById("root"));
// }
