import React from "react";
import ReactDOM from "react-dom";
import { useHistory } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Input } from "antd";
import { AudioOutlined } from "@ant-design/icons";

const { Title } = Typography;
const { Panel } = Collapse;
const { Search } = Input;

import Axios from "axios";
import Cookies from "js-cookie";

// const InputSearch = () => {
//     const suffix = (
//         <AudioOutlined
//             style={{
//                 fontSize: 16,
//                 color: "#1890ff"
//             }}
//         />
//     );

//     const onSearch = value => {
//         Axios.get(`/api/search-section/?query=${value}`)
//             .then(response => {
//                 // console.log(response);
//                 const { data, status } = response;

//                 console.log(data);

//                 if (status == 200) {
//                     setSections(data);
//                 } else {
//                     alert("Jaringan Error / Gagal");
//                 }
//             })
//             .catch(error => {
//                 alert(error);
//             });
//     };

//     return (
//         <>
//             <Search
//                 placeholder="input search text"
//                 allowClear
//                 enterButton="Search"
//                 size="large"
//                 onSearch={onSearch}
//             />
//         </>
//     );
// };

const List = () => {
    const [sections, setSections] = React.useState([]);
    const history = useHistory();
    React.useEffect(() => {
        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
        Axios.get("/api/section")
            .then(response => {
                // console.log(response);
                const { data, status } = response;

                console.log(data);

                if (status == 200) {
                    setSections(data);
                } else {
                    alert("Jaringan Error / Gagal");
                }
            })
            .catch(error => {
                alert(error);
            });
        }
    }, []);

    const handleDelete = async id => {
        if (window.confirm("yakin mau dihapus?")) {
            try {
                const response = await Axios.delete("api/section/" + id);
                alert("data berhasil dihapus");
                // sections.splice(sections.indexOf(response.data.id), 1);
                //  updateList(list.filter(item => item.name !== name));
                setSections(sections.filter(section => section.id !== id));
                history.push("/");
            } catch (error) {
                alert("Network error");
            }
        }
    };

    const InputSearch = () => {
        const suffix = (
            <AudioOutlined
                style={{
                    fontSize: 16,
                    color: "#1890ff"
                }}
            />
        );

        const onSearch = value => {
            Axios.get(`/api/search-section/?query=${value}`)
                .then(response => {
                    // console.log(response);
                    const { data, status } = response;

                    console.log(data);

                    if (status == 200) {
                        setSections(data);
                    } else {
                        alert("Jaringan Error / Gagal");
                    }
                })
                .catch(error => {
                    alert(error);
                });
        };

        return (
            <>
                <Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={onSearch}
                />
            </>
        );
    };

    return (
        <Row>
            <Col span={20} offset={2}>
                <Title level={2}>List Sections</Title>
                <a href="/add-section">
                    <Button type="primary" className={"add-section"}>
                        Add Section
                    </Button>
                </a>
                <a href="/user/profile/after-log" style={{marginLeft: 10}}>
                    <Button type="primary" className={"add-section"}>
                        Profile
                    </Button>
                </a>
                <Col span={12}>
                    <InputSearch />
                </Col>
                <hr />
                <Collapse accordion>
                    {sections &&
                        sections.map((section, index) => {
                            return (
                                <Panel header={section.description} key={index}>
                                    <a href={`/edit-section/${section.id}`}>
                                        <Button
                                            type="primary"
                                            className={"button-section"}
                                        >
                                            Update Section
                                        </Button>
                                    </a>
                                    <Button
                                        type="primary"
                                        className={"button-section"}
                                        onClick={() => handleDelete(section.id)}
                                        danger
                                    >
                                        Delete Section
                                    </Button>
                                    <a href={`/section/${section.id}`}>
                                        <Button
                                            type="primary"
                                            className={"button-section"}
                                        >
                                            Task Section
                                        </Button>
                                    </a>
                                    {/* <p>{section.description}</p> */}
                                </Panel>
                            );
                        })}
                </Collapse>
            </Col>
        </Row>
    );
};

export default List;

// if (document.getElementById("root")) {
//     ReactDOM.render(<App />, document.getElementById("root"));
// }
