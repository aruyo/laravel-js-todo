<?php

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now();
        $dateNow = $dt->toDateTimeString();

        DB::table('tasks')->insert([
            'id_section' => 1,
            'description' => 'Buy Cofee',
            'is_done' => 1,
            'done_at' => $dateNow,
            'created_at' => $dateNow
        ]);

        DB::table('tasks')->insert([
            'id_section' => 1,
            'description' => 'doing homework',
            'created_at' => $dateNow
        ]);
        DB::table('tasks')->insert([
            'id_section' => 1,
            'description' => 'watching movies',
            'created_at' => $dateNow
        ]);

        DB::table('tasks')->insert([
            'id_section' => 2,
            'description' => 'Buy Milk',
            'is_done' => 1,
            'done_at' => $dateNow,
            'created_at' => $dateNow
        ]);

        DB::table('tasks')->insert([
            'id_section' => 2,
            'description' => 'cleaning the house',
            'created_at' => $dateNow
        ]);
        DB::table('tasks')->insert([
            'id_section' => 2,
            'description' => 'Playing PUBG',
            'created_at' => $dateNow
        ]);
    }
}
