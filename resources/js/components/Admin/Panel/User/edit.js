import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Edit = () => {
    const history = useHistory();
    const [user, setUser] = React.useState({
        name: "",
        email: "",
        password: ""
    });

    const { userId } = useParams();

    React.useEffect(() => {
        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
        Axios.get(`/api/user/${userId}`)
            .then(response => {
                // console.log(response);
                const { status, data } = response;
                if (status == 200) {
                    setUser(data);
                    console.log(data);
                    form.setFieldsValue({
                        name: data.name,
                        email: data.email,
                    });
                } else {
                    alert("Error !");
                }
            })
            .catch(error => {
                alert(error);
            });
        }
    }, [userId]);

    const [form] = Form.useForm();

    const handleChange = (e, name) => {
        const value = e.target.value;
        setUser({
            ...user,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.put(`/api/user/update/${userId}`, user);

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("update user berhasil");
                history.push(`/admin/users`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Please input your user!");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Create New User</Title>
                <hr />
                <br />
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                    form={form}
                >
                    <Form.Item label="Name" name="name">
                        <Input
                            value={user.name}
                            onChange={e => handleChange(e, "name")}
                        />
                    </Form.Item>
                    <Form.Item label="Email" name="email">
                        <Input
                            value={user.email}
                            onChange={e => handleChange(e, "email")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                        <a href="/" className={"back-button"}>
                            <Button type="primary" className={"add-user"}>
                                Back
                            </Button>
                        </a>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Edit;
