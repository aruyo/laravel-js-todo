## How-to

**Docker compose build**
```bash
docker-compose build
```
**Docker comopose run app**
```bash
docker-compose up
```
**Install Package Javascript**
```bash
npm install / yarn install
```
**Run Laravel MIX (React)**
```bash
npm run dev && npm run watch
```
**Install Package PHP**
```bash
composer install
```
**Check Artisan List**
```bash
php artisan list
```
**Database Migration**
```bash
php artisan migrate
```
**Seeding Database**
```bash
php artisan db:seed
```
**Artisan Command New Task**
```bash
php artisan task:create
```
**Artisan Command Change Status Task**
```bash
php artisan task:change
```
