import React from "react";
import ReactDOM from "react-dom";
import { useHistory } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Add = () => {
    const history = useHistory();
    const [user, setUser] = React.useState({
        password: ""
    });

    const handleChange = (e, name) => {
        const value = e.target.value;
        setUser({
            ...user,
            [name]: value
        });
    };
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let email = params.get("email");
    let token = params.get("token");
    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.post(
                `/api/reqpassword?email=${email}&token=${token}`,
                user
            );

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                // alert("tambah user berhasil");
                alert(response.data);
                history.push(`/user/login`);
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Error !");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Form Change Password</Title>
                <hr />
                <br />
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                >
                    <Form.Item label="Password" name="password">
                        <Input.Password
                            value={user.password}
                            onChange={e => handleChange(e, "password")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Add;
