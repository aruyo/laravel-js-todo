<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});


Route::get('users', 'UserController@listUser');
Route::get('user/{id}', 'UserController@showUser');
Route::post('user/register', 'UserController@register');
Route::put('user/update/{id}', 'UserController@updateUser');
Route::delete('user/delete/{id}', 'UserController@deleteUser');
Route::get('confirm', 'UserController@confirm');
Route::post('me', 'UserController@me');

//forgot
Route::post('reqpassword', 'UserController@confirmForgot');
Route::post('forgot', 'UserController@requestForgot');
// Section Route
Route::get('section/{id}/task', 'SectionController@task');
Route::get('search-section', 'SectionController@search');
Route::resource('section', 'SectionController');

// Task Route
Route::get('search-task/{id}', 'TaskController@search');
Route::get('filter-task/{id}', 'TaskController@filter');
Route::resource('task', 'TaskController');
