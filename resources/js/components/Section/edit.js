import React from "react";
import ReactDOM from "react-dom";
import { useHistory, useParams } from "react-router-dom";
import { Collapse, Row, Col, Typography, Button, Form, Input } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import Axios from "axios";
import Cookies from "js-cookie";

const layout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 6,
        span: 16
    }
};

const Edit = () => {
    const history = useHistory();
    const { sectionId } = useParams();
    const [section, setSection] = React.useState({
        description: ""
    });
    const [form] = Form.useForm();

    React.useEffect(() => {
        if (!Cookies.get("login")) {
            history.push(`/user/login`);
        } else {
        Axios.get(`/api/section/${sectionId}`)
            .then(response => {
                // console.log(response);
                const { status, data } = response;
                if (status == 200) {
                    setSection(data);
                    console.log(data);
                    form.setFieldsValue({
                        description: data.description
                    });
                } else {
                    alert("Error !");
                }
            })
            .catch(error => {
                alert(error);
            });
        }
    }, [sectionId]);

    const handleChange = (e, name) => {
        const value = e.target.value;
        setSection({
            ...section,
            [name]: value
        });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const response = await Axios.put(
                `/api/section/${sectionId}`,
                section
            );

            console.log(response);

            // const { status, message } = response.data;
            if (response.status == 200) {
                alert("update section berhasil");
                history.push("/");
            } else {
                alert(message);
            }
        } catch (error) {
            alert("Network error");
        }
    };

    return (
        <Row>
            <Col span={12} offset={6}>
                <Title level={2}>Edit Section</Title>
                <hr />
                <Form {...layout} form={form}>
                    <Form.Item label="Description" name="description">
                        <Input
                            type="text"
                            onChange={e => handleChange(e, "description")}
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" onClick={handleSubmit}>
                            Submit
                        </Button>
                        <a href="/" className={"back-button"}>
                            <Button type="primary" className={"add-section"}>
                                Back
                            </Button>
                        </a>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Edit;

// if (document.getElementById("root")) {
//     ReactDOM.render(<App />, document.getElementById("root"));
// }
