<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\registerEmail;
use App\Mail\forgotPassword;
use App\Mail\deleteUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use ReallySimpleJWT\Token;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    private $userId = 12;
    private $secret = 'sec!ReT423*&';
    private $issuer = 'localhost';

    public function register(Request $request)
    {
        $data = new User;
        $data->name = $request->get('name');
        $data->email = $request->get('email');
        $data->password = Hash::make($request->get('password'));
        $data->save();

        $expiration = time() + 3600;
        $token = Token::create($this->userId, $this->secret, $expiration, $this->issuer);

        $dataMail = [
            'id' => $data->id,
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'token' => $token

        ];

        Mail::to($request->get('email'))->send(new registerEmail($dataMail));
        return response()->json($dataMail);
    }

    public function confirm(Request $request)
    {
        $email = $request->input('email');
        $token = $request->input('token');

        $result = Token::validate($token, $this->secret);

        $dt = Carbon::now();
        $dateNow = $dt->toDateTimeString();

        if ($result === true) {
            $data = User::where('email', $email)->first();
            $data->email_verified_at = $dateNow;
            $data->save();
        }

        return response()->json('thank !');
    }

    public function requestForgot(Request $request)
    {

        $expiration = time() + 3600;
        $token = Token::create($this->userId, $this->secret, $expiration, $this->issuer);

        $email = $request->get('email');

        $data = [
            'token' => $token,
            'email' => $email
        ];
        Mail::to($request->get('email'))->send(new forgotPassword($data));
        return response()->json($token);
    }

    public function forgot(Request $request)
    {
        // $data = $request->input('email');
        $token = $request->input('token');

        $result = Token::validate($token, $this->secret);

        if ($result === true) {
            return view('form-req');
        }
    }

    public function confirmForgot(Request $request)
    {
        $email = $request->input('email');
        $token = $request->input('token');

        $result = Token::validate($token, $this->secret);

        if ($result == true) {
            $data = User::where('email', $email)->first();
            $data->password = Hash::make($request->get('password'));
            $data->save();
        }
        return response()->json('thank!');
    }

    public function listUser()
    {
        $data = User::all();

        return response()->json($data);
    }

    public function showUser($id)
    {
        $data = User::findOrFail($id);

        return response()->json($data);
    }

    public function updateUser(Request $request, $id)
    {
        $data = User::findOrFail($id);
        $data->name = $request->get('name');
        $data->email = $request->get('email');
        $data->password = Hash::make($request->get('password'));
        $data->save();

        return response()->json($data);
    }

    public function deleteUser($id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        Mail::to($data->email)->send(new deleteUser($data));
        return response()->json($data);
    }

    public function me(Request $request)
    {
        // $token = $request->header('Authorization');
        // return response()->json($token);
        $token = $request->get('token');
        $email = $request->get('email');
        $result = Token::validate($token, $this->secret);
        if ($result === true) {
            $data = User::where('email', $email)->first();

            return response()->json($data);
        }
    }
}
