import React from "react";
import ReactDOM from "react-dom";
import { Button } from "antd";

import "antd/dist/antd.css";

function App() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-body">Mau Ganti Lagi !!!</div>
                        <Button type="primary">Primary Button</Button>
                        <Button>Default Button</Button>
                        <Button type="dashed">Dashed Button</Button>
                        <br />
                        <Button type="text">Text Button</Button>
                        <Button type="link">Link Button</Button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;

if (document.getElementById("root")) {
    ReactDOM.render(<App />, document.getElementById("root"));
}
