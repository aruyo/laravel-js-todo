<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;

class createTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Task from Artisan Command Line';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Input the Section ID : ";
        $ID = trim(fgets(STDIN));
        echo "Input the Task Description : ";
        $task = trim(fgets(STDIN));
        $data = new Task;
        $data->id_section = $ID;
        $data->description = $task;
        $data->save();
        $this->info('Task has been added!');
        //
    }
}
